﻿/*Problem 1:
Description:
    Given a list of people with their birth and end years (all between 1900 and 2000), find the year with the most 
    number of people alive.
Code:
    Solve using a language of your choice and dataset of your own creation.
Submission:
    Please upload your code, dataset, and example of the program’s output to Bit Bucket or Github. Please include any 
    graphs or charts created by your program.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class TheLiveliestYear
    {
        //list of lifespans, which is a struct that has the birth and death year of the person
        public List<lifespan> persons = new List<lifespan>();

        //This is the start and end of the dataset
        public int startYear = 1900;
        public int endYear = 2000;

        static void Main(string[] args)
        {
            int theLiveliestYear;
            //Create Data Set 
            TheLiveliestYear tly = new TheLiveliestYear();
            tly.MakeDataSet(500);
            theLiveliestYear = tly.EvaluateDataSet(tly.persons.ToArray());

            //Display the Year with the most people alive
            Console.WriteLine("\n");
            Console.WriteLine("The liveliest year is " + theLiveliestYear);
            Console.WriteLine("\n");
            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
        
        /// <summary>
        /// Struct that holds the birth and death year
        /// </summary>
        public struct lifespan
        {
            public int birthYear;
            public int deathYear;
        }

        /// <summary>
        /// Class that make a list of random lifespans 
        /// </summary>
        /// <param name="numberOfSamples"></param> - this is the sample size
        private void MakeDataSet(int numberOfSamples)
        {
            //Create new random Number generator and lifespan struct
            Random randYear = new Random();
            lifespan life = new lifespan();

            //Make a list of lifespans based off the numberofsamples passed in
            for (int i = 0; i < numberOfSamples; i++)
            {
                int a = randYear.Next(startYear, endYear);
                int b = randYear.Next(startYear, endYear);

                //if the first random number is greater than the second make it the death year
                //if they are equal it does not matter you can set it either way
                if (a >= b)
                {
                    life.birthYear = b;
                    life.deathYear = a;
                }
                else
                {
                    life.birthYear = a;
                    life.deathYear = b;
                }

                //Console.WriteLine(life.birthYear + "-" + life.deathYear);

                //Add the lifespan to the list of lifespans for latter evaluation
                persons.Add(life);
            }

        }

        /// <summary>
        /// Function that takes in a list of lifespans, calculates how many people where alive for each year 1900-2000 and returns
        /// the year with the most people alive
        /// </summary>
        /// <param name="lifespans"> This is the data set of birth and death years</param> 
        /// <returns>The year with the most people alive</returns>
        private int EvaluateDataSet(lifespan[] lifespans)
        {
            int[] peopleAliveDuringYear = new int[(endYear - startYear)+1];


            //Calculate for each year
            for (int i = 0; i < peopleAliveDuringYear.Length; i++)
            {
                int counter = 0;
                //Each year we have to see if each person was alive
                for (int j = 0; j < lifespans.Length; j++)
                {
                    //Check to see the peson was alive during the year in question
                    if ((lifespans[j].birthYear <= (startYear + i)) && (lifespans[j].deathYear >= (startYear + i)))
                    {
                        counter++;
                    } 
                }

                Console.WriteLine("Year: " + (startYear + i) + " - People Alive : " + counter);
                peopleAliveDuringYear[i] = counter;
            }

            return (startYear + Array.IndexOf(peopleAliveDuringYear, peopleAliveDuringYear.Max()));
        }
    }
}
