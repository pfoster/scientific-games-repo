﻿/*Problem 1:
Description:
    Given a list of people with their birth and end years (all between 1900 and 2000), find the year with the most 
    number of people alive.
Code:
    Solve using a language of your choice and dataset of your own creation.
Submission:
    Please upload your code, dataset, and example of the program’s output to Bit Bucket or Github. Please include any 
    graphs or charts created by your program.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestQuestion1
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Values for input boxes
        /// </summary>
        int startyear = 0;
        int endYear = 0;
        int sampleSize = 0;

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is the calculate year Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            //This clears out the points from any previous calculations
            this.chart1.Series["People Alive"].Points.Clear();

            TheLiveliestYear liveliest = new TheLiveliestYear();
            //This is an array of how many people where alive, it starts at the start year
            int[] dataPoints;
          
            if (Int32.TryParse(this.textBox1.Text, out startyear) && Int32.TryParse(this.textBox2.Text, out endYear) && Int32.TryParse(this.textBox3.Text, out sampleSize)) 
            {
                this.chart1.ChartAreas[0].AxisX.Minimum = startyear;
                this.chart1.ChartAreas[0].AxisX.Maximum = endYear;

                //Get calculated int array
                dataPoints = liveliest.CreateGraphPoints(startyear, endYear, sampleSize);

                //Make a point for each year on the line graph
                for (int i = 0; i < dataPoints.Length; i++)
                {
                    this.chart1.Series["People Alive"].Points.AddXY(startyear + i, dataPoints[i]);
                }

                //Display the year as a popup box
                MessageBox.Show("The year with most people alive was " + (startyear + Array.IndexOf(dataPoints, dataPoints.Max())));
            }
        }
        
    }

    /// <summary>
    /// This class gets random birth and death years and calculates how many people where alive for each year
    /// </summary>
    class TheLiveliestYear
    {
        //list of lifespans, which is a struct that has the birth and death year of the person
        public List<lifespan> persons = new List<lifespan>();

        //This is the start and end of the dataset
        public int startYear = 1900;
        public int endYear = 2000;
        public int sampleSize = 500;

        public int[] CreateGraphPoints(int start, int end, int sampSize)
        {
            startYear = start;
            endYear = end;
            sampleSize = sampSize;
            
            //Create Data Set 
            TheLiveliestYear tly = new TheLiveliestYear();
            tly.MakeDataSet(sampleSize);

            //evaluate and return array to form so that we can populate the graph
            return tly.EvaluateDataSet(tly.persons.ToArray());
        }

        /// <summary>
        /// Struct that holds the birth and death year
        /// </summary>
        public struct lifespan
        {
            public int birthYear;
            public int deathYear;
        }

        /// <summary>
        /// Class that make a list of random lifespans 
        /// </summary>
        /// <param name="numberOfSamples"></param> - this is the sample size
        private void MakeDataSet(int numberOfSamples)
        {
            //Create new random Number generator and lifespan struct
            Random randYear = new Random();
            lifespan life = new lifespan();

            //Make a list of lifespans based off the numberofsamples passed in
            for (int i = 0; i < numberOfSamples; i++)
            {
                int a = randYear.Next(startYear, endYear);
                int b = randYear.Next(startYear, endYear);

                //if the first random number is greater than the second make it the death year
                //if they are equal it does not matter you can set it either way
                if (a >= b)
                {
                    life.birthYear = b;
                    life.deathYear = a;
                }
                else
                {
                    life.birthYear = a;
                    life.deathYear = b;
                }

                //Console.WriteLine(life.birthYear + "-" + life.deathYear);

                //Add the lifespan to the list of lifespans for latter evaluation
                persons.Add(life);
            }

        }

        /// <summary>
        /// Function that takes in a list of lifespans, calculates how many people where alive for each year 1900-2000 and returns
        /// the year with the most people alive
        /// </summary>
        /// <param name="lifespans"> This is the data set of birth and death years</param> 
        /// <returns>The year with the most people alive</returns>
        private int[] EvaluateDataSet(lifespan[] lifespans)
        {
            int[] peopleAliveDuringYear = new int[(endYear - startYear) + 1];

            //Calculate for each year
            for (int i = 0; i < peopleAliveDuringYear.Length; i++)
            {
                int counter = 0;
                //Each year we have to see if each person was alive
                for (int j = 0; j < lifespans.Length; j++)
                {
                    //Check to see the peson was alive during the year in question
                    if ((lifespans[j].birthYear <= (startYear + i)) && (lifespans[j].deathYear >= (startYear + i)))
                    {
                        counter++;
                    }
                }

                //Console.WriteLine("Year: " + (startYear + i) + " - People Alive : " + counter);
                peopleAliveDuringYear[i] = counter;
            }

            return peopleAliveDuringYear;
        }
    }
}
